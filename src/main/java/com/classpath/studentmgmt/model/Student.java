package com.classpath.studentmgmt.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;

@Builder
@Entity
public class Student {

    public Student(){

    }

    public Student(long id, String name, int grade, Set<Address> addressSet, Set<Course> courses) {
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.addressSet = addressSet;
        this.courses = courses;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "student_name", nullable = false )
    private String name;


    @Column(name="student_grade", nullable = false)
    private int grade;

    @OneToMany(mappedBy = "student", cascade = ALL, fetch = EAGER)
    private Set<Address> addressSet = new HashSet<>();

    @ManyToMany(mappedBy = "students", cascade = ALL, fetch = EAGER)
    private Set<Course> courses = new HashSet<>();

    public void addAddress(Address address){
        this.addressSet.add(address);
        address.setStudent(this);
    }

    public void addCourse(Course course){
        this.courses.add(course);
        course.getStudents().add(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return id == student.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public Set<Address> getAddressSet() {
        return addressSet;
    }

    public void setAddressSet(Set<Address> addressSet) {
        this.addressSet = addressSet;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", grade=" + grade +
                ", addressSet=" + addressSet +
                ", courses=" + courses +
                '}';
    }
}