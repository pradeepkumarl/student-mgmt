package com.classpath.studentmgmt.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

import static javax.persistence.GenerationType.AUTO;

@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = AUTO)
    private long addressId;

    private String addressLine;

    private String city;

    private String state;

    private String zipCode;

    @ManyToOne
    @JoinColumn(name="student_id", nullable = false)
    @JsonIgnore
    private Student student;

}