package com.classpath.studentmgmt.service;

import com.classpath.studentmgmt.model.Student;
import com.classpath.studentmgmt.repository.StudentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;


@AllArgsConstructor
/**
 * This class will perform the business validation and save the state of <code>{@link Student}</code> to the Database.
 * @author Pradeep Kumar
 * @Version 1.0
 *
 */
@Service
public class StudentService {

    private StudentRepository studentRepository;

    public Student save(Student student){
        System.out.println("Came inside the Student Service save method");
        if (validateStudent(student)) {
            return this.studentRepository.save(student);
        }
        return null;
    }

    private boolean validateStudent(Student student) {
        if (student.getCourses().size() < 2){
            return false;
        }
        return true;
    }

    public Set<Student> findAll(){
        return new HashSet<>(this.studentRepository.findAll());
    }

    public Set<Student> findStudentsByName(String name){
        List<Student> students = this.studentRepository.findByNameIgnoreCase(name);
        return new HashSet<>(students);
    }

    public Student findById(long studentId){
        return this.studentRepository
                .findById(studentId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid student id"));
    }

    public Student update(long studentId, Student student) {

        Optional<Student> optionalStudent = this.studentRepository.findById(studentId);
        if (optionalStudent.isPresent()) {
            Student studentFromDAO = optionalStudent.get();
            studentFromDAO.setGrade(student.getGrade());
            studentFromDAO.setName(student.getName());
           return this.studentRepository.save(studentFromDAO);
        } else {
            throw new IllegalArgumentException("Student with the id does not exists");
        }
    }

    /**
     * This function deletes the student by the given Id
     * @param studentId
     * @throws IllegalArgumentException
     * <code>{@link IllegalArgumentException}</code>
     */
    public void deleteStudentById(long studentId) {
        Optional<Student> optionalStudent = this.studentRepository.findById(studentId);
        //if the student is present, delete else throw IllegalArgument Exception
        if (optionalStudent.isPresent()){
            this.studentRepository.deleteById(studentId);
        } else {
            throw new IllegalArgumentException(" Invalid Student id to be deleted ");
        }
    }



    public List<Student> findByName(String name) {
        return this.studentRepository.findByNameIgnoreCase(name);
    }
}