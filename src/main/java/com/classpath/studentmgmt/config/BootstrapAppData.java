package com.classpath.studentmgmt.config;

import com.classpath.studentmgmt.model.Address;
import com.classpath.studentmgmt.model.Course;
import com.classpath.studentmgmt.model.Student;
import com.classpath.studentmgmt.repository.StudentRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;

import java.util.HashSet;
import java.util.Set;

@Slf4j
@AllArgsConstructor
@Configuration
public class BootstrapAppData implements ApplicationListener<ApplicationReadyEvent> {

    private StudentRepository studentRepository;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
       log.info("===== Application started ======");


        Address rameshCommunicationAddress = Address.builder().addressLine("141, NStreet").state("TamilNadu").city("Chennai").zipCode("577142").build();
        Address rameshMailingAddress = Address.builder().addressLine("141, NStreet").state("TamilNadu").city("Chennai").zipCode("588741").build();

        Address sureshCommunicationAddress = Address.builder().addressLine("141, NStreet").state("TamilNadu").city("Chennai").zipCode("577142").build();
        Address sureshMailingAddress = Address.builder().addressLine("141, NStreet").state("TamilNadu").city("Chennai").zipCode("588741").build();

        Course maths = Course.builder().name("Maths").students(new HashSet<>()).build();
        Course physics = Course.builder().name("Physics").students(new HashSet<>()).build();

        Course english = Course.builder().name("English").students(new HashSet<>()).build();
        Course hindi = Course.builder().name("Hindi").students(new HashSet<>()).build();

        Student ramesh = Student.builder().grade(5).name("Ramesh").addressSet(new HashSet<>()).courses(new HashSet<>()).build();
        Student suresh = Student.builder().grade(9).name("Suresh").addressSet(new HashSet<>()).courses(new HashSet<>()).build();

        ramesh.addCourse(maths);
        ramesh.addCourse(physics);
        ramesh.addAddress(rameshCommunicationAddress);
        ramesh.addAddress(rameshMailingAddress);

        suresh.addCourse(english);
        suresh.addCourse(hindi);
        suresh.addAddress(sureshCommunicationAddress);
        suresh.addAddress(sureshMailingAddress);

        //save the student
        this.studentRepository.save(ramesh);
        this.studentRepository.save(suresh);
        log.info("===== Application initialized ======");
    }
}