package com.classpath.studentmgmt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class StudentMgmtApplication {
	public static void main(String[] args) {
		SpringApplication.run(StudentMgmtApplication.class, args);
	}
}
