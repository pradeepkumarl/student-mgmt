package com.classpath.studentmgmt.repository;

import com.classpath.studentmgmt.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    List<Student> findByNameIgnoreCase(String name);

    List<Student> findByNameLikeAndGradeGreaterThan(String name, int grade);

    @Query("select student from Student student where student.grade >= ?1")
    Student fetchByGrade(int grade);

}