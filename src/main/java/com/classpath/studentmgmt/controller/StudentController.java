package com.classpath.studentmgmt.controller;

import com.classpath.studentmgmt.model.Student;
import com.classpath.studentmgmt.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Set;

@RestController
@AllArgsConstructor
@RequestMapping("/api/students")
public class StudentController {

    private StudentService studentService;

    private RestTemplate restTemplate;

    @PostMapping
    public Student saveStudent(@RequestBody Student student){
        System.out.println("Came inside the Student Controller save method");
        student.getAddressSet().forEach(address -> address.setStudent(student));
        student.getCourses().forEach(course -> course.getStudents().add(student));
        //make a rest call to onboarding service
        this.restTemplate.postForEntity("http://ONBOARDINGSERVICE/api/registration", student, Student.class);
        return this.studentService.save(student);
    }

    @GetMapping
    public Set<Student> fetchAll(){
        return this.studentService.findAll();
    }

    @GetMapping("/{id}")
    public Student findById(@PathVariable("id") long studentId){
        return this.studentService.findById(studentId);
    }

    @PutMapping("/{id}")
    public Student updateStudentById(@PathVariable("id") long studentId, @RequestBody Student student){
        return this.studentService.update(studentId, student);
    }

    @DeleteMapping("/{id}")
    public void deleteStudentById(@PathVariable("id") long studentId){
        this.studentService.deleteStudentById(studentId);
    }

    @GetMapping("/name/{name}")
    public List<Student> fetchByName(@PathVariable("name") String name){
        return this.studentService.findByName(name);
    }
}