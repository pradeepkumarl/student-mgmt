package com.classpath.studentmgmt.model;

import org.junit.jupiter.api.Test;
import java.util.HashSet;
import static org.junit.jupiter.api.Assertions.*;
public class StudentTest {

    @Test
    public void testDefaultConstructor(){
        Student student = new Student();
        assertNotNull(student);
        assertEquals(student.getId(), 0);
        assertNotNull(student.getAddressSet());
        assertEquals(student.getAddressSet().size(), 0);
        assertNotNull(student.getCourses());
        assertNull(student.getName());
    }

    @Test
    public void testAddCourse(){
        //data set creation
        Course maths = Course.builder().name("Maths").courseId(1).students(new HashSet<>()).build();
        Course science = Course.builder().name("Science").courseId(2).students(new HashSet<>()).build();
        Student student = new Student();
        //execution
        student.addCourse(maths);
        student.addCourse(science);
        //verification
        assertNotNull(student.getCourses());
        assertEquals(student.getCourses().size(), 2);
        assertEquals(maths.getStudents().size(), 1);
        assertEquals(science.getStudents().size(), 1);
    }

    @Test
    public void testAllArgConstructor(){
         Student student = new  Student(12, "Ramesh", 8, new HashSet<>(), new HashSet<>());
         assertEquals(student.getId(), 12);
         assertEquals(student.getName(), "Ramesh");
         assertEquals(student.getGrade(), 8);
         assertEquals(student.getCourses().size(),0);
         assertEquals(student.getAddressSet().size(),0);
    }

    @Test
    public void testToString(){
        Student student1 = new  Student(12, "Ramesh", 8, new HashSet<>(), new HashSet<>());
        assertEquals(student1.toString(), "Student{id=12, name='Ramesh', grade=8, addressSet=[], courses=[]}");
    }

    @Test
    public void testEquals(){
        Student student1 = new  Student(12, "Ramesh", 8, new HashSet<>(), new HashSet<>());
        Student student2 = new  Student(12, "Suresh", 8, new HashSet<>(), new HashSet<>());

        assertTrue(student1.equals(student2));
    }

    @Test
    public void testNotEquals(){
        Student student1 = new  Student(12, "Ramesh", 8, new HashSet<>(), new HashSet<>());
        Student student2 = new  Student(13, "Suresh", 8, new HashSet<>(), new HashSet<>());
        assertFalse(student1.equals(student2));
    }
}