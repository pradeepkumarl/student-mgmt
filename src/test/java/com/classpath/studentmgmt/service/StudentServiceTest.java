package com.classpath.studentmgmt.service;

import com.classpath.studentmgmt.model.Course;
import com.classpath.studentmgmt.model.Student;
import com.classpath.studentmgmt.repository.StudentRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.*;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class StudentServiceTest {

    @Mock
    private StudentRepository studentRepository;

    @InjectMocks
    private StudentService studentService;

    Set<Course> courses = new HashSet<>();
    Student ramesh;

    Student suresh;

    @BeforeEach
    public void setupTestData(){
        Course maths = Course.builder().name("Maths").courseId(1).students(new HashSet<>()).build();
        Course science = Course.builder().name("Science").courseId(2).students(new HashSet<>()).build();
        Course physics = Course.builder().name("Physics").courseId(3).students(new HashSet<>()).build();

        courses = new HashSet<>();
        courses.add(maths);
        courses.add(science);
        courses.add(physics);

       ramesh = new  Student(12, "Ramesh", 8, new HashSet<>(), courses);

       suresh = new  Student(13, "Suresh", 8, new HashSet<>(), courses);
    }

    @Test
    public void testSaveStudentWithInvalidCourses(){
        Student student = new  Student(0, "Ramesh", 8, new HashSet<>(), new HashSet<>());
        Student savedStudent = new  Student(12, "Ramesh", 8, new HashSet<>(), new HashSet<>());

        //executing
        Student returnedStudent = studentService.save(student);

        //assertions
        assertNull(returnedStudent);
    }

    @Test
    public void testSaveStudentWithValidCourses(){


        Student student = new  Student(0, "Ramesh", 8, new HashSet<>(), courses);
        Student savedStudent = new  Student(12, "Ramesh", 8, new HashSet<>(), courses);

        //setting the expectation on the mock
        when(studentRepository.save(student)).thenReturn(savedStudent);

        //executing
        Student returnedStudent = studentService.save(student);

        //assertions
        assertNotNull(returnedStudent);
        assertEquals(returnedStudent.getId(), 12);

        //verification
        verify(studentRepository, times(1)).save(student);
    }


    @Test
    public void testFindAllStudents(){
        when(studentRepository.findAll()).thenReturn(Arrays.asList(ramesh, suresh));

        //executing
        Set<Student> students = studentService.findAll();

        assertNotNull(students);
        assertEquals(students.size(), 2);

        //verification
        verify(studentRepository, times(1)).findAll();

    }

    @Test
    public void testFindByInvalidStudentId(){
        when(studentRepository.findById(12L)).thenReturn(Optional.empty());

        //executing
        try {
            Student student = studentService.findById(12L);
            fail("Expected an exception of IllegalArgument");
        } catch (Exception exception){
            assertNotNull(exception);
            assertTrue(exception instanceof IllegalArgumentException);
            assertEquals(exception.getMessage(), "Invalid student id");
        }
        verify(studentRepository, times(1)).findById(12L);
    }

    @Test
    public void testFindByInvalidStudentIdUsingFunctionalStyle(){
        when(studentRepository.findById(12L)).thenReturn(Optional.empty());
        assertThrows(IllegalArgumentException.class, () -> {
            studentService.findById(12L);
        });
        verify(studentRepository, times(1)).findById(12L);
    }


    @Test
    public void testFindByValidStudentId(){
        when(studentRepository.findById(12L)).thenReturn(Optional.of(ramesh));

        //executing
        try {
            Student student = studentService.findById(12L);
            assertNotNull(student);
        } catch (Exception exception){
            fail("Should not come to the exception block::");
        }

        verify(studentRepository, times(1)).findById(12L);

    }

    @Test
    public void testDeleteByInValidStudentId(){
        when(studentRepository.findById(12L)).thenReturn(Optional.empty());
        //executing
        try {
            studentService.deleteStudentById(12L);
            fail("Should not come to the exception block::");
        } catch (Exception exception){
            assertNotNull(exception);
            assertTrue(exception instanceof IllegalArgumentException);
        }
        verify(studentRepository, times(1)).findById(12L);
        verify(studentRepository, never()).deleteById(12L);
    }

    @Test
    public void testDeleteByValidStudentId(){
        when(studentRepository.findById(12L)).thenReturn(Optional.of(ramesh));
        //executing
        try {
            studentService.deleteStudentById(12L);
        } catch (Exception exception){
            fail("Should not come to the exception block::");
        }
        verify(studentRepository, times(1)).findById(12L);
        verify(studentRepository, times(1)).deleteById(12L);
    }

    @Test
    public void testFindInvalidStudentByName(){
        //executing
        Set<Student> students = studentService.findStudentsByName("ramesh");
        assertNotNull(students);
    }

    @Test
    public void testFindValidStudentByName(){
        //executing
        List<Student> studentList = new ArrayList<>();
        studentList.add(Student.builder().name("ramesh").build());
        when(studentRepository.findByNameIgnoreCase("ramesh")).thenReturn(studentList);
        Set<Student> students = studentService.findStudentsByName("ramesh");
        assertNotNull(students);
        assertTrue(!students.isEmpty());
        assertEquals(students.size(), 1);
        verify(studentRepository, times(1)).findByNameIgnoreCase("ramesh");
    }

    @Test
    public void testInvalidUpadateStudent(){
        //executing
        when(studentRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(IllegalArgumentException.class, () -> {
            studentService.update(1L, Student.builder().name("ramesh").build());
        });
        verify(studentRepository, times(1)).findById(1L);
        verify(studentRepository, never()).save(any());

    }

    @Test
    public void testValidUpadateStudent(){
        //executing
        when(studentRepository.findById(1L)).thenReturn(Optional.of( Student.builder().name("ramesh").grade(4).addressSet(new HashSet<>()).courses(new HashSet<>()).build()));
        Student toBeUpdated = Student.builder().name("suresh").grade(5).build();
        when(studentRepository.save(any())).thenReturn(toBeUpdated);
        Student updatedStudent = studentService.update(1L, toBeUpdated);
        assertNotNull(updatedStudent);
        assertEquals(updatedStudent.getName(), toBeUpdated.getName());
        assertEquals(updatedStudent.getGrade(), toBeUpdated.getGrade());
        verify(studentRepository, times(1)).findById(1L);
        verify(studentRepository, times(1)).save(toBeUpdated);
    }

}